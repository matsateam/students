package net.ukr.wumf;

import java.util.Arrays;

public class Group {
	private String nameGroup;
	private Student[] array = new Student[10];

	public Group(String nameGroup) {
		super();
		this.nameGroup = nameGroup;
	}

	public Group() {
		super();
	}

	public String getNameGroup() {
		return nameGroup;
	}

	public void setNameGroup(String nameGroup) {
		this.nameGroup = nameGroup;
	}

	public Student[] getArray() {
		return array;
	}

	public void setArray(Student[] array) {
		this.array = array;
	}

	public void addStudent(Student st) throws GroupIsFullException {
		for (int i = 0; i < array.length; i++) {
			if (array[i] == null) {
				array[i] = st;
				break;
			}
			if (i == array.length - 1) {
				throw new GroupIsFullException("������ ��� ������");
			}
		}
	}

	public void deleteStudent(String id) {
		boolean check = true;
		for (int i = 0; i < array.length; i++) {
			if (array[i] != null) {
				if (id.equals(array[i].getId())) {
					array[i] = null;
					for (int k = i; k < array.length - 1; k++) {
						Student replace = array[k];
						array[k] = array[k + 1];
						array[k + 1] = replace;
					}
					check = false;
					break;
				}
			}
		}
		System.out.println((check) ? "������ �������� ��� � �������" : "������� c id " + id + " ������");
	}

	public Student findStudent(String lastName) {
		for (int i = 0; i < array.length; i++) {
			if (array[i] != null) {
				if (lastName.equals(array[i].getLastName())) {
					return array[i];
				}
			}
		}
		return null;
	}

	@Override
	public String toString() {
		String result = "Group " + nameGroup + "\n";
		String[] arr = new String[array.length];

		for (int i = 0; i < arr.length; i++) {
			if (array[i] != null) {
				arr[i] = array[i].getLastName() + " " + array[i].getName();
			} else {
				arr[i] = "";
			}
		}
		Arrays.sort(arr);
		for (int k = 0; k < arr.length; k++) {
			if (arr[k] != null && arr[k] != "") {
				String[] find = arr[k].split(" ");
				result += findStudent(find[0]).toString() + "\n";
			}
		}
		return result;
	}

}
