package net.ukr.wumf;

public class Student extends Human {
	private String groupName;
	private String id;

	public Student(String name, String lastName, int age, String groupName, String id) {
		super(name, lastName, age);
		this.groupName = groupName;
		this.id = id;
	}

	public Student() {
		super();
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "������� " + groupName + ", id = " + id + " " + ", " + super.toString();
	}
}
