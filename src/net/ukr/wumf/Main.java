package net.ukr.wumf;

public class Main {

	public static void main(String[] args) {

		Student stud1 = new Student("������", "��������", 25, "G10", "1");
		Student stud2 = new Student("����", "������", 22, "G10", "2");
		Student stud3 = new Student("������", "��������", 23, "G10", "3");
		Student stud4 = new Student("�����", "��������", 22, "G10", "4");
		Student stud5 = new Student("����", "�������",24, "G10", "5");
		Student stud6 = new Student("���", "���������", 21, "G10", "6");
		Student stud7 = new Student("������", "��������", 27, "G10", "7");
		Student stud8 = new Student("�����", "������", 23, "G10", "8");
		Student stud9 = new Student("���", "����", 22, "G10", "9");
		Student stud10 = new Student("����", "�����������", 21, "G10", "10");
		Student stud11 = new Student("����", "��������", 22, "G11", "11");

		Group g10 = new Group("G10");
		Group g11 = new Group("G11");
		

		try {
			g10.addStudent(stud1);
			g10.addStudent(stud2);
			g10.addStudent(stud3);
			g10.addStudent(stud4);
			g10.addStudent(stud5);
			g10.addStudent(stud6);
			g10.addStudent(stud7);
			g10.addStudent(stud8);
			g10.addStudent(stud9);
			g10.addStudent(stud10);
			
		} catch (GroupIsFullException e) {
			e.printStackTrace();
		}
		System.out.println(g10);
		
		g10.deleteStudent("7");

		if (g10.findStudent("��������") == null) {
			System.out.println("�������� ��� � �������");
		} else {
			System.out.println(g10.findStudent("�������"));
		}
			
		System.out.println();
		try {
			g11.addStudent(stud11);
		} catch (GroupIsFullException e) {
			e.printStackTrace();
			
		}
		System.out.println(g11);

	}
}