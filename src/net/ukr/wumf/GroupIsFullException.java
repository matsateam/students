package net.ukr.wumf;

public class GroupIsFullException extends Exception {

	private static final long serialVersionUID = 1L;

	public GroupIsFullException() {
		super();
	}

	public GroupIsFullException(String message) {
		super(message);
	}

}
